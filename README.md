# README #

## Introduction ##
This repository contains C++ code for haplotype reconstruction of time-series viral data.

## Contents ##
The repository contains the following folders:

 * Code: Contains all the C++ code for performing haplotype reconstruction.
 * Sample_data: Contains a sample fluB dataset sampled at 41 time points. Input files in the SAMFIRE format have been generated.
 * Sample_output: Contains haplotype reconstruction output based on the sample data.
 * Scripts: Contains scripts for submitting haplotype reconstruction jobs at the Cambridge HPC system. Also contains perl scripts for manipulation of fasta files.

## Prerequisites ##

This C++ program requires use of functions defined in the GNU Scientific Library (GSL). Before attempting to download the program, please ensure there is a recent version of GSL installed. GSL may be downloaded from [https://www.gnu.org/software/gsl/](https://www.gnu.org/software/gsl/). Please note down the installation paths.

## Compilation ##
The code may be compiled using the command `make all`. It is likely that the makefile needs updating prior to this, e.g. for updating the location of the GSL library. The executable is `realDataWH.run`.

## Contact ##

Please address any questions to Chris Illingworth, [chris.illingworth@gen.cam.ac.uk](mailto:chris.illingworth@gen.cam.ac.uk).