#!/bin/bash

###
### This file describes 20 analysis seeds. This file feeds information to the slurm setup file (slurm_single_analysis_SL2_1_core.sh). The slurm setup file defines how to run this on the Cambridge HPC cluster. This need to be updated to work with your system.
###

#Change directory to output folder. This is where the slurm-XXXXXXX.out file is written to. This file contains the output from the analysis. This is specific to the Cambridge HPC system.
cd /home/ckl35/haplotype-reconstruction/Sample_output



#Define the range of analysis seeds
for seed in `seq 1001 1020`; do


	
	#SBATCH the slurm setup file slurm_single_analysis_SL2_1_core.sh with a list of command line arguments. See README for details. This is specific to the Cambridge HPC system.
	sbatch /home/ckl35/haplotype-reconstruction/Scripts/Haplotype_Reconstruction/slurm_single_analysis_SL2_1_core.sh -if /home/ckl35/haplotype-reconstruction/Sample_data/FluB_multi_locus_data_150419 -of /home/ckl35/haplotype-reconstruction/Sample_output/Hap_Inference_DirMult_C200_150419 -as $seed -C 200


done

