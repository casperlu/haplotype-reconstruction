#cluster_fa.pl

#Removes sequences with ambiguous 'N' nucleotides 

#Clusters sequences in a file so that no two sequences differ by less than some cutoff number of nucleotides, d.

#Usage: perl cluster_fa.pl -i <input file> -o <output file in .fa format> -d <distance threshold>

#Sequences must be aligned before use of this code. Gaps in the alignment are ignored when calculating the threshold distance.

#The input file must be in .fasta format in which the sequence of nucleotides is contained in a single line.  The accompanying code process_fa.pl will achieve this task

#!/usr/bin/perl -w

use strict;
use Getopt::Std;

my %OPTS;
getopts('i:d:o:nsm',\%OPTS);

my $file = $OPTS{"i"};
my $out = $OPTS{"o"};
my $d = $OPTS{"d"};
my $FQ_FILE;

open (FQ_FILE, "< $file") or die "Cannot open $file";
open (FA_FILE, "> $out") or die "Cannot open $out";
my $index=0;
my $num=0;
my $header;
my $seq='';
my @seqs;
my @heads;
my $len=0;
print "Reading data...\n";
while (<FQ_FILE>) {
  chomp;
  my @words = split;
  my @chars=split(//,$_);
  $len=@chars;
  if ($chars[0] eq '>') {
    push(@heads,$_);
  } else {
    my @chars = split(//,$_);
    $len=@chars;
    push @seqs, [split(//,$_)];
  }
}
my @del;
print "Parsing data...\n";
my $size=@seqs;
print "$size $len\n";
for (my $i=0;$i<$size;$i++) {
  push(@del,0);
}

for (my $i=0;$i<$size;$i++) {
  for (my $k=0;$k<$len;$k++) {
    if ($seqs[$i][$k] eq "N") {
      $del[$i]=1;
    }
  }
  if ($del[$i]==0){
    for (my $j=$i+1;$j<$size;$j++) {
      if ($del[$j]==0) {    
        my $diff=0;    
        for (my $k=0;$k<$len;$k++) {
          if ($seqs[$i][$k] ne "-" && $seqs[$j][$k] ne "-") {
            if ($seqs[$i][$k] ne $seqs[$j][$k]) {
              $diff++;
            }
          }
        }
        if ($diff<$d) {
          $del[$j]=1;
        }
      }
    }
  }
} 

for (my $i=0;$i<$size;$i++) {
  if ($del[$i]==0) {
    print FA_FILE "@heads[$i]\n";
    for (my $j=0;$j<$len;$j++) {
      print FA_FILE "$seqs[$i][$j]";
    }
    print FA_FILE "\n";
  }
}
