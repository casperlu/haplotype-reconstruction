#find_diffs.pl

#Given two input files in .fasta format, this identifies sequences in the second file which the closest in terms of sequence distance to each sequence in the first file

#Usage: perl find_diffs.pl -i <first input file> -s <second input file> -o <output file in .fa format> -d <additional distance threshold>

#Basic usage: Suppose that the first file contains three sequences.  For each sequence the code will identify the nearest sequence in the second set and report this.  If more than one sequence in the second set is minimally equidistant from a sequence, this will also be reported.

#Advanced usage: If the distance threshold is set to any value greater than zero, this includes in the output any sequence within this value of the minimum distance.  For example, if the minimum distance between a sequence in the first file and any sequence in the second file is three nucleotides, the option -d 2 will mean that the code returns all sequences in the second file with a distance of 3+2=5 nucleotides of the sequence in the first file.

#Sequences must be aligned before use of this code. Gaps in the alignment are ignored when calculating the threshold distance.

#The input file must be in .fasta format in which the sequence of nucleotides is contained in a single line.  The accompanying code process_fa.pl will achieve this task


#Compares sequences to those in file s

#!/usr/bin/perl -w

use strict;
use Getopt::Std;

my %OPTS;
getopts('i:d:o:s:nsm',\%OPTS);

my $compare = $OPTS{"s"};

my $COMP_FILE;
open (COMP_FILE, "< $compare") or die "Cannot open comparison set\n";

my @heads_c;
my @seqs_c;
my $len;
print "Reading comparison file...\n";
while (<COMP_FILE>) {
  chomp;
  my @words = split;
  my @chars=split(//,$_);
  $len=@chars;
  if ($chars[0] eq '>') {
    push(@heads_c,$_);
  } else {
    my @chars = split(//,$_);
    $len=@chars;
    push @seqs_c, [split(//,$_)];
  }
}

my $seqs = $OPTS{"i"};
my $out = $OPTS{"o"};
my $dist= $OPTS{"d"};
my $FQ_FILE;

open (FQ_FILE, "< $seqs") or die "Cannot open $seqs";
my $index=0;
my $num=0;
my $header;
my $seq='';
my @seqs;
my @heads;
my $len=0;
print "Reading data...\n";
while (<FQ_FILE>) {
  chomp;
  my @words = split;
  my @chars=split(//,$_);
  $len=@chars;
  if ($chars[0] eq '>') {
    push(@heads,$_);
  } else {
    my @chars = split(//,$_);
    $len=@chars;
    push @seqs, [split(//,$_)];
  }
}


print "Calculating distances...\n";
my $size=@seqs;
my $size_c=@seqs_c;
my @diffs;
#print "$size $size_c\n";
my @dist;
for (my $i=0;$i<$size;$i++) {
  my $ref=0;
  my $min_diff=10000;
  for (my $j=0;$j<$size_c;$j++) {
    my $diff=0;
    for (my $k=0;$k<$len;$k++) {
 #     print "$seqs[$i][$k] $seqs_c[$j][$k] $diff\n"; 
      if ($seqs[$i][$k] ne "-" && $seqs_c[$j][$k] ne "-") {
        if ($seqs[$i][$k] ne "N" && $seqs_c[$j][$k] ne "N") {
          if ($seqs[$i][$k] ne $seqs_c[$j][$k]) {
	    $diff++;
          }
        }
      }
    }
    if ($diff<$min_diff) {
      $min_diff=$diff;
      $ref=$j;
    }
  }
  push(@diffs,$min_diff);
#  print "$heads[$i]\n";
#  print "$heads_c[$ref]\n";
#  print "$i $min_diff $ref\n";
}

my $len=@diffs;
my $min_len=1000;
for (my $i=0;$i<$len;$i++) {
  if ($diffs[$i]<$min_len) {
    $min_len=$diffs[$i];
  }
}

print "Minimum distance $min_len\n";

my @min_d;
for (my $i=0;$i<$len;$i++) {
  if ($diffs[$i]<=$min_len+$dist) {
    push(@min_d,$i);
    print "$heads[$i]\n";
    for (my $j=0;$j<$len;$j++) {
      print "$seqs[$i][$j]";
    }
    print "\n";
  }
}



