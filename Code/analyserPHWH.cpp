//
//  analyserPHWH.cpp
//
//

#include "analyserPHWH.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include "misc.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <limits>
#include <math.h>
#include <gsl/gsl_linalg.h>	
#include <algorithm>

using namespace std;

AnalyserPHWH::AnalyserPHWH() { } //Constructor does nothing
AnalyserPHWH::~AnalyserPHWH() { } //Destructor does nothing

void AnalyserPHWH::loadData(Data *d) {
    
    //cout << "Loading data for partial haplotypes. \n";
    data = dynamic_cast<DataPH *>(d);
}

void AnalyserPHWH::runAnalysis(AnaParam *ap) {
    
	//This method needs to get updated (if ever in use again)
	cout << "Running analysis for partial haplotypes. - CURRENTLY NOT IMPLEMENTED!\n";
    
}

void AnalyserPHWH::optimiseqMean(int numFHs, gsl_rng *r, int C, vector<vector<double> >* qMean, vector<double>* pL) {

	int dim = numFHs;
	qMean->clear();
	pL->clear(); //pointer to Likelihoods


	for(int t=0; t<data->getNumOfTimePoints(); t++) { //Loop over time points, assuming that all SOPHs have the same time points

		cout << "\n\nOptimising qMean for time point " << t << ":\n";
		vector<double> qMeanTimePoint; //qMean for time point t

		//Initialise mean to random values, then renormalise
		for(int i=0; i<dim; i++) {

			qMeanTimePoint.push_back(gsl_rng_uniform_pos(r));
		}
		rescale(qMeanTimePoint);

  
		//Define "new" parameters
		vector<double> qMeanTimePointNew = qMeanTimePoint;

	
		double bestL = - std::numeric_limits<double>::max();
		double delta = 1;
		int accepts = 0;
		int trys = 0;
		int nUpdate = 1000; //Frequency for updating acceptance rate


		int k=0; //Counter
		while(delta > 0.000001) {
			k++;
			if(k%nUpdate==0) {

				double acceptanceRate=(accepts+0.)/(trys+0.);
				//cout << "Acceptance rate: " << acceptanceRate << "\tdelta: " << delta << "\n";


				double tempDelta=delta*(0.95+acceptanceRate);
				double tempnUpdate = ceil(nUpdate*(0.95+acceptanceRate));
				if(tempDelta < 10) { // If delta>10 computations get out of ahnd
					delta = tempDelta;
					if(tempnUpdate > 100 && tempnUpdate <= 1000) { nUpdate = tempnUpdate; }

					cout << "Delta: " << delta << "\tnUpdate: " << nUpdate << "\tL: " << bestL << "\n";
				}

				//Reset
				accepts=0;
				trys=0;
			}
	
			//Update mean
			addRandom(qMeanTimePoint,qMeanTimePointNew,delta,r); //Create a new vector newFreqs from adding a small delta*rndm(double) to a random index

//DirMult method is the standard, as much faster and more correct!
//			double newL = computeLmean(&qMeanTimePointNew, r, C, t, true); 
			double newL = computeLmeanDirMult(&qMeanTimePointNew, C, t, false); 


			if(newL > bestL) {

				qMeanTimePoint = qMeanTimePointNew;

				bestL = newL;
				accepts++;
			
		
			} else {
				//Revert back any changes
				qMeanTimePointNew = qMeanTimePoint;
			}	
		
			trys++;
		}



		qMean->push_back(qMeanTimePoint);
		pL->push_back(bestL);
	}
} 



//Input mean of full dimensionality	
double AnalyserPHWH::computeLmean(vector<double>* qMean, gsl_rng* r, double C, int timeIndex, bool print) {

	double L = 0;
	int numSOPHs = data->getNumOfSOPHs();
	int dimFH = (int) qMean->size();


	for(int i=0; i<numSOPHs; i++) { //Loop over sets of partial haps

		DataPH::setOfPartialHaps* SOPHi = data->getSOPH(i);

		if(print == true) {
			cout << "SOPH " << i << "\n";
		}
		int dimPH = SOPHi->getNumOfPartialHaps();
        	vector<int> obs = SOPHi->getObs(timeIndex); //This might be a slow process. Consider making more efficient.
		int Ntot = sumOfVector(obs);
		if(print == true) {

			cout << "obs: "; printIntVector(obs);
		}

		if(Ntot == 0) { 
	
			//This SOPH has Ntot of zero for time point i, so not informative. As such we ignore it.
			continue;
		}

		vector<vector<int> > contribsPHset;
		for(int j=0; j<dimPH; j++) {

			contribsPHset.push_back(*(SOPHi->getContribs(j)));
		}


		//Construct matrix T
		gsl_matrix* T = gsl_matrix_alloc(dimPH,dimFH);
		for(int j=0; j<dimPH; j++) { //Loop over rows 

			if(print == true) {
					
				cout << "contribs PH set " << j << ": "; printIntVector(contribsPHset[j]);
			}

			int counter = 0;
			for(int k=0; k<dimFH; k++) { //Loop over columns

				if(contribsPHset[j][counter]==k && counter < (int) contribsPHset[j].size()) { //This assumes the contribs are ordered

					gsl_matrix_set(T,j,k,1);
					counter++;
				} else {
					gsl_matrix_set(T,j,k,0);
				}
			}
		}


		/*
		 * First, transform qMean into qMean_PH=TqMean
		 */
		//Turn qMean into gsl vector
		gsl_vector *qMeanGSL = gsl_vector_alloc(dimFH);
		for(int i=0;i<dimFH;i++) {
			gsl_vector_set(qMeanGSL,i,(*qMean)[i]);
		}

		//Create TqBmean through matrix vector multiplication
		gsl_vector *TqMeanGSL = gsl_vector_alloc(dimPH);
		gsl_blas_dgemv(CblasNoTrans, 1.0, T, qMeanGSL, 0.0, TqMeanGSL);

		//Convert back into C++ vector
		vector<double> TqMean;
		vector<double> NtotTqMean;
		for(int i=0; i<dimPH; i++) {
			TqMean.push_back(gsl_vector_get(TqMeanGSL, i));
			NtotTqMean.push_back(Ntot*TqMean[i]);
		}

	
		/*
		 * Next, create the variance expression 
		 */
		//Construct epsilonNtotM(TqMean) with epsilon = (Nb+C)/(1+C)
		gsl_matrix* epsilonNtotMTqMean = gsl_matrix_alloc(dimPH,dimPH);
		constructMatrixM(TqMean, epsilonNtotMTqMean);
		double epsilon = (Ntot+C)/((double)(1+C));
		gsl_matrix_scale(epsilonNtotMTqMean, epsilon*Ntot);

		
		double LphSet = - numeric_limits<double>::max();

		if(dimPH > 1) { //Most common case, i.e. multiple partial haps in a set
		
			//Reduce dimensionality by 1 to ensure non-degeneracy
			NtotTqMean.pop_back();
			gsl_matrix_view qVarPHreducedView = gsl_matrix_submatrix(epsilonNtotMTqMean, 0,0, dimPH-1, dimPH-1); //Remove row k and column k
			gsl_matrix * qVarPH = gsl_matrix_alloc(dimPH-1, dimPH-1);
			gsl_matrix_memcpy(qVarPH, &(qVarPHreducedView.matrix)); //Copy the reduced matrix view into a "normal" matrix varXaPH
			vector<double> obsReduced;
			for(int i=0; i<dimPH-1; i++) {
				obsReduced.push_back(obs[i]);
	        	}


			if(dimPH>2) { //Multivariate approach

				LphSet = logMultivariateNormalPDF(obsReduced,NtotTqMean, qVarPH);
		
			} else if(dimPH==2) {

				//Univariate case
				double qMeanUni = NtotTqMean[0];
				double qVarUni = gsl_matrix_get(qVarPH,0,0);
				double stDevqVarUni = sqrt(qVarUni);

				LphSet = -(obs[0]-qMeanUni)*(obs[0]-qMeanUni)/(2*qVarUni) - log(stDevqVarUni*sqrt(2*M_PI)); //log L
	
			}

			//Deallocate variables created in this scope			
			gsl_matrix_free(qVarPH);

		} else if (dimPH == 1) { 
			//Case where there is a single partial haplotype in a set.
			//This can be thought of as a case with a single observed partial haplotype
			//and one (or several) unobserved haplotypes.
			//If all loci in the partial haplotype set are variants (i.e. not monomorphic)
			//then there MUST exists at least one other haplotype containing the unobserved alleles,
			//however, this (or these) haplotype(s) are not observed.
			//As such, WLOG we can consider this a case of dimPH=2, which gets reduced to a one dimensional system
			//under reduction, i.e. similar to the dimPH==2 case of above.
				
			double qMeanUni = NtotTqMean[0];
			double qVarUni = gsl_matrix_get(epsilonNtotMTqMean,0,0);
			double stDevqVarUni = sqrt(qVarUni);

			LphSet = -(obs[0]-qMeanUni)*(obs[0]-qMeanUni)/(2*qVarUni) - log(stDevqVarUni*sqrt(2*M_PI)); //log L
		
	
		} else {

			cout << "ERROR in computeLmean: dimPH is <1\n";
		}

		if(print == true) {
			cout << "LphSet: " << LphSet << "\n";
		}


		//Check if L is still negative infinity (shouldn't happen)
		if(LphSet == -numeric_limits<double>::max() || ::isnan(LphSet) == true) { 
		
				cout << "ERROR in AnalyserPH::computeLmean: Likelihood is negative infinity.\n";
				exit(1);
		}
		else{

			L += LphSet;
		}

		//Clean up
		gsl_vector_free(qMeanGSL);
		gsl_vector_free(TqMeanGSL);
		gsl_matrix_free(T);
		gsl_matrix_free(epsilonNtotMTqMean);

	}

	return L;
} 



//Input mean of full dimensionality	
double AnalyserPHWH::computeLmeanDirMult(vector<double>* qMean, double C, int timeIndex, bool print) {

	double L = 0;
	int numSOPHs = data->getNumOfSOPHs();
	int dimFH = (int) qMean->size();


	for(int i=0; i<numSOPHs; i++) { //Loop over sets of partial haps

		DataPH::setOfPartialHaps* SOPHi = data->getSOPH(i);

		if(print == true) {
			cout << "SOPH " << i << "\n";
		}
		int dimPH = SOPHi->getNumOfPartialHaps();
        	vector<int> obs = SOPHi->getObs(timeIndex); //This might be a slow process. Consider making more efficient.
		int Ntot = sumOfVector(obs);
		if(print == true) {

			cout << "obs: "; printIntVector(obs);
		}

		if(Ntot == 0) { 
	
			//This SOPH has Ntot of zero for time point i, so not informative. As such we ignore it.
			continue;
		}

		vector<vector<int> > contribsPHset;
		for(int j=0; j<dimPH; j++) {

			contribsPHset.push_back(*(SOPHi->getContribs(j)));
		}


		//Construct matrix T
		gsl_matrix* T = gsl_matrix_alloc(dimPH,dimFH);
		for(int j=0; j<dimPH; j++) { //Loop over rows 

			if(print == true) {
					
				cout << "contribs PH set " << j << ": "; printIntVector(contribsPHset[j]);
			}

			int counter = 0;
			for(int k=0; k<dimFH; k++) { //Loop over columns

				if(contribsPHset[j][counter]==k && counter < (int) contribsPHset[j].size()) { //This assumes the contribs are ordered

					gsl_matrix_set(T,j,k,1);
					counter++;
				} else {
					gsl_matrix_set(T,j,k,0);
				}
			}
		}


		/*
		 * Construct alpha=C*qMean_PH = C*T*qMean
		 */
		//Turn qMean into gsl vector
		gsl_vector *qMeanGSL = gsl_vector_alloc(dimFH);
		for(int i=0;i<dimFH;i++) {
			gsl_vector_set(qMeanGSL,i,(*qMean)[i]);
		}

		//Create TqBmean through matrix vector multiplication
		gsl_vector *TqMeanGSL = gsl_vector_alloc(dimPH);
		gsl_blas_dgemv(CblasNoTrans, 1.0, T, qMeanGSL, 0.0, TqMeanGSL);

		//Convert back into C++ vector
		vector<double> alpha;
		for(int i=0; i<dimPH; i++) {
			alpha.push_back(C*gsl_vector_get(TqMeanGSL, i));
		}

		
		double LphSet = - numeric_limits<double>::max();

		if(dimPH > 1) { //Most common case, i.e. multiple partial haps in a set
			
			if(dimPH>2) { //Multivariate approach, i.e. Dirichlet Multinomial likelihood

				LphSet = logDirMultProb(alpha, obs, Ntot);

		
			} else if(dimPH==2) { //Univariate case, i.e. beta binomial

				//alpha, beta, x, n
				LphSet = logBetaBinProb(alpha[0], alpha[1], obs[0], Ntot);

	
			}


		} else if (dimPH == 1) { 
			//Case where there is a single partial haplotype in a set.
			//This can be thought of as a case with a single observed partial haplotype
			//and one (or several) unobserved haplotypes.
			//If all loci in the partial haplotype set are variants (i.e. not monomorphic)
			//then there MUST exists at least one other haplotype containing the unobserved alleles,
			//however, this (or these) haplotype(s) are not observed.
			//As such, WLOG we can consider this a case of dimPH=2, i.e. similar to the dimPH==2 case of above.
				

			//alpha=Cq
			//beta=C(1-q)=C-alpha
			double beta = C - alpha[0];

			//alpha, beta, x, n
			LphSet = logBetaBinProb(alpha[0], beta, obs[0], Ntot);
		
	
		} else {

			cout << "ERROR in computeLmean: dimPH is <1\n";
		}


		//Check if L is still negative infinity (shouldn't happen)
		if(LphSet == -numeric_limits<double>::max() || ::isnan(LphSet) == true) { 
		
				cout << "ERROR in AnalyserPHWH::computeLmean: Likelihood is negative infinity.\n";
				exit(1);
		}
		else{

			L += LphSet;
		}

		//Clean up
		gsl_vector_free(qMeanGSL);
		gsl_vector_free(TqMeanGSL);
		gsl_matrix_free(T);

	}

	return L;
} 


//Haplotype construction methods
void AnalyserPHWH::constructFullHaps(vector<Sequence> *phs, vector<Sequence> *fhs) {
    
    //Changing sequences in vectors of chars to work with Chris' code
 //   cout << "Partial haps:\n";
    vector<vector<char> > haps;
    for(unsigned int i=0; i<phs->size(); i++) {
        
    //    (*phs)[i].print();
        
        vector<char> h;
        for(int j=0; j< (*phs)[i].getLength(); j++) {
            h.push_back((*phs)[i].getBase(j));
        }
        
        haps.push_back(h);
    }

    
    //Cycle through overlap steps
    vector<vector<char> > haps2=haps;
//    for(unsigned int i=0; i<haps.size();i++) {
//        cout << "Char converted partial hap is: ";
//        for(unsigned int j=0; j<haps[i].size();j++) {
//            cout << haps[i][j];
//        }
//        cout << "\n";
//    }
    
    //Matching overlap joins
    for (int i=0;i<200;i++) { //Change to while loop and include a stopping criteria?
        haps2=haps;
        OverlapStepShared1(haps); //Remove haplotypes contained within each other
        OverlapStepShared2(haps);
        if (haps==haps2) {
            break;
        }
    }
    
    //Non-matching overlap joins
    for (int i=0;i<20;i++) {
        haps2=haps;
        OverlapStepNoShare(haps);
        OverlapStepShared1(haps);
        OverlapStepShared2(haps);
        if (haps==haps2) {
            break;
        }
    }
    
    
    
    //Non-overlap joins
    vector<AnalyserPHWH::par> sf;
    GetStartFinish(sf,haps);
    vector< vector<char> > new_haps;
    BuildPaths(sf,haps,new_haps);
    haps=new_haps;
    DuplicateStep(haps);
    
    
    //Converting from vector<char> to Sequence
  //  cout << "Full haps:\n";
    for(unsigned int i=0; i<haps.size(); i++) {
        Sequence s;
        for(unsigned int j=0; j<haps[i].size(); j++) {
            s.addBase(haps[i][j]);
        }
        //s.print();
        fhs->push_back(s);
    }
    
    
}

//Method for updating haps following a step (e.g. overlap step, combination step, etc.)
void AnalyserPHWH::ResetHaps (vector<int> incl, vector< vector<char> >& haps) {
    vector< vector<char> > new_haps;
    for (unsigned int i=0;i<haps.size();i++) {
        if (incl[i]==1) {
            new_haps.push_back(haps[i]);
        }
    }
    haps=new_haps;
}


//Remove duplicate haplotypes
void AnalyserPHWH::DuplicateStep (vector< vector<char> >& haps) {
    vector< vector<char> > new_haps;
    vector<int> uniq;
    for (unsigned int i=0;i<haps.size();i++) {
        uniq.push_back(1);
    }
    //cout << "Haplotypes " << haps.size() << "\n";
    for (unsigned int i=0;i<haps.size();i++) { //Simply loop over all combinations i,j>i and check if any of them are duplicates
        for (unsigned int j=i;j<haps.size();j++) {
            if (i!=j&&haps[i]==haps[j]) {
                uniq[j]=0;
            }
        }
    }
    for (unsigned int i=0;i<haps.size();i++) {
        if (uniq[i]==1) {
            new_haps.push_back(haps[i]);
        }
    }
    // cout << "Unique haplotypes " << new_haps.size() << "\n";
    haps=new_haps;
}

//Remove haplotypes contained within each other
void AnalyserPHWH::OverlapStepShared1 (vector< vector<char> >& haps) {
    //One contained within another - try doing these first before other overlaps...
    vector<int> incl (haps.size(),1); //Haplotypes that should be kept have a 1, those to be removed have a 0
    int size=haps.size();
    for (int i=0;i<size;i++) {
        for (int j=0;j<size;j++) {
            if (i!=j) {
                int match=1;   //Match at shared positions
                int n_match=0;  //Number of shared positions
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    //Check whether j is contained in i.
                    if (haps[j][k]!='-') { //Here we are only considering the alleles k in hap j that isn't '-'
                        if (haps[i][k]!='-') {
                            n_match++;
                        }
                        if (haps[j][k]!=haps[i][k]) { //If just a single one of the non-'-' alleles in hap j is not identical to hap i, then no match
                            match=0;
                        }
                    }
                }
                if (match==1&&n_match>0) {
                    //Remove haps[j]
                    incl[j]=0;
                }
            }
        }
    }
    ResetHaps(incl,haps); //Reset haps, i.e. only keep haps with a 1 in incl
    DuplicateStep(haps); //Remote any possible duplicate haplotypes
}




//Remove shared overlap like AACC-- --CCTT
void AnalyserPHWH::OverlapStepShared2 (vector< vector<char> >& haps) {
    vector<int> incl (haps.size(),1);
    int size=haps.size();
    for (int i=0;i<size;i++) {
        for (int j=i+1;j<size;j++) {
            int match=1;   //Match at shared positions
            int n_match=0;  //Number of shared positions
            for	(unsigned int k=0;k<haps[i].size();k++) {
                //Check whether i and j have the same nucleotides wherever they overlap.
                if (haps[j][k]!='-'&&haps[i][k]!='-') { //Number of overlaps
                    n_match++;
                    if (haps[j][k]!=haps[i][k]) {
                        match=0;
                    }
                }
            }
            if (match==1&&n_match>0) {
//                for	(int k=0;k<haps[i].size();k++) {
//                    cout << haps[i][k];
//                }
//                cout << " overlaps ";
//                for	(int k=0;k<haps[j].size();k++) {
//                    cout << haps[j][k];
//                }
//                cout << "\n";
                vector<char> newhap;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[j][k]!='-') {
                        newhap.push_back(haps[j][k]);
                    } else if (haps[i][k]!='-') {
                        newhap.push_back(haps[i][k]);
                    } else {
                        newhap.push_back('-');
                    }
                }
                //Add newhap, remove the other two
                haps.push_back(newhap);
                incl.push_back(1);
                incl[i]=0;
                incl[j]=0;
            }
        }
    }
    
    //Update haps and remove any possible duplicates
    ResetHaps(incl,haps);
    DuplicateStep(haps);
}


//Non-matching overlaps
void AnalyserPHWH::OverlapStepNoShare (vector< vector<char> >& haps) {
    
    vector<int> incl (haps.size(),1);
    int size=haps.size();
    for (int i=0;i<size;i++) {
        for (int j=i+1;j<size;j++) {
            int match=1;   //Match at shared positions
            int n_match=0;  //Number of shared positions
            for	(unsigned int k=0;k<haps[i].size();k++) {
                //Check whether i and j have the same nucleotides wherever they overlap.  N.B. Must have at least one locus not in common
                if (haps[j][k]!='-'&&haps[i][k]!='-') { //Number of overlaps
                    n_match++;
                    if (haps[j][k]!=haps[i][k]) {
                        match=0;
                    }
                }
            }
            int n_over=0;
            if (match==0&&n_match<(int) haps[i].size()&&n_match>0) { //some shared positions, but not a complete match
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[j][k]!='-'&&haps[i][k]=='-') {
                        n_over=1; //There is an overlap of some kind
                    }
                    if (haps[i][k]!='-'&&haps[j][k]=='-') {
                        n_over=1; //There is an overlap of some kind
                    }
                }
            }
            
            //Overlap present
            if (n_over==1) {
                
                vector<char> nh_start;
                //Make beginning
                int ov=0;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[i][k]!='-'&&haps[j][k]!='-') {
                        ov=1;
                    }
                    if (ov==0) {
                        if (haps[j][k]!='-') {
                            nh_start.push_back(haps[j][k]);
                        } else if (haps[i][k]!='-') {
                            nh_start.push_back(haps[i][k]);
                        } else {
                            nh_start.push_back('-');
                        }
                    } else {
                        nh_start.push_back('-');
                    }
                }
                
                //Make end
                vector<char> nh_end;
                ov=0;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[i][k]!='-'&&haps[j][k]!='-') {
                        ov=1;
                    }
                    if (ov==1) {
                        if (haps[j][k]!='-'&&haps[i][k]=='-') {
                            nh_end.push_back(haps[j][k]);
                        } else if (haps[i][k]!='-'&&haps[j][k]=='-') {
                            nh_end.push_back(haps[i][k]);
                        } else {
                            nh_end.push_back('-');
                        }
                    } else {
                        nh_end.push_back('-');
                    }
                }
                
                //Make c1 and c2
                vector<char> nh_c1;
                vector<char> nh_c2;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (nh_start[k]=='-'&&nh_end[k]=='-') {
                        nh_c1.push_back(haps[i][k]);
                        nh_c2.push_back(haps[j][k]);
                    } else {
                        nh_c1.push_back('-');
                        nh_c2.push_back('-');
                    }
                }
                
                //Construct start + c1 + end
                vector<char> newhap1;
                vector<char> newhap2;
                for (unsigned int k=0;k<haps[i].size();k++){
                    if (nh_start[k]!='-') {
                        newhap1.push_back(nh_start[k]);
                        newhap2.push_back(nh_start[k]);
                    } else if (nh_c1[k]!='-') {
                        newhap1.push_back(nh_c1[k]);
                        newhap2.push_back(nh_c2[k]);
                    } else if (nh_end[k]!='-') {
                        newhap1.push_back(nh_end[k]);
                        newhap2.push_back(nh_end[k]);
                    } else {
                        newhap1.push_back('-');
                        newhap2.push_back('-');
                    }
                }
                
                haps.push_back(newhap1);
                haps.push_back(newhap2);
                incl.push_back(1);
                incl.push_back(1);
                //cout << newhap1.size() << " " << newhap2.size() << "\n";
                incl[i]=0;
                incl[j]=0;
            }
        }
    }
    ResetHaps(incl,haps);
    DuplicateStep(haps);
}

void AnalyserPHWH::GetStartFinish(vector<AnalyserPHWH::par>& sf, vector< vector<char> >& haps) {
    for (unsigned int i=0;i<haps.size();i++) {
        AnalyserPHWH::par p;
        p.i1=-1;
        p.i2=-1;
        for (unsigned int j=0;j<haps[i].size();j++) {
            //cout << "i " << i << " j " << j << " " << haps[i][j] << "\n";
            if (haps[i][j]!='-'&&p.i1<0) {
                p.i1=j;
                //	cout << "Found start\n";
            }
            if (haps[i][j]=='-'&&p.i1>=0&&p.i2<0) {
                p.i2=j-1;
                //	cout << "Found end\n";
            }
        }
        if (p.i2<0) {
            p.i2=haps[i].size();
        }
        sf.push_back(p);
    }
}


void AnalyserPHWH::AddPath (int prev, int start, vector<char> v, vector<AnalyserPHWH::par>& sf, vector< vector<char> >& haps, vector< vector<char> >& new_haps) {
    //Add last haplotype to vector
    vector<char> v2=v;
    //cout << "Previous is " << prev << " " << sf[prev].i1 << " " << sf[prev].i2 << "\n";
    for (int j=sf[prev].i1;j<=sf[prev].i2;j++) {
        v2.push_back(haps[prev][j]);
    }
    //cout << "Vector v is: ";
    //for (int i=0;i<v2.size();i++) {
    //	cout << v2[i];
    //}
    //cout << "\n";
    
    //Check if this is the end of the haplotype and if so push to new_haps
    if (sf[prev].i2>=(int) haps[prev].size()) {
        //	cout << "Doing push_back\n";
        vector<char> v3;
        for (unsigned int i=0;i<haps[prev].size();i++) {//Trim length
            v3.push_back(v2[i]);
        }
        new_haps.push_back(v3);
    } else {
        //If not search for new haplotype and recall
        for (unsigned int i=0;i<sf.size();i++) {
            if (sf[i].i1==start) {
                AddPath(i,sf[i].i2+1,v2,sf,haps,new_haps);
            }
        }
    }
}

void AnalyserPHWH::BuildPaths(vector<AnalyserPHWH::par>& sf, vector< vector<char> > haps, vector< vector<char> >& new_haps) {
    vector<char> v;
    for (unsigned int i=0;i<sf.size();i++) {
        if (sf[i].i1==0) {
            //cout << "Detected " << i << "\n";
            AddPath(i,sf[i].i2+1,v,sf,haps,new_haps);
        }
    }
}



