//
//  analyserPHWH.hpp
//
//

#ifndef analyserPHWH_hpp
#define analyserPHWH_hpp

#include <stdio.h>
#include "data.h"
#include "analyser.h"
#include "dataPH.hpp"
#include "gsl/gsl_rng.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

class AnalyserPHWH : public Analyser {
private:
    DataPH *data;
    
public:
    AnalyserPHWH(); //Constructor
    ~AnalyserPHWH(); //Deconstructor
    void loadData(Data *d);
    void runAnalysis(AnaParam *ap);
    
        
	// * = slightly deprecated, but kept for completeness

	void optimiseqMean(int numFHs, gsl_rng *r, int C, std::vector<std::vector<double> >* qMean, std::vector<double>* pL); 

	//Likelihood calculators
    double computeLmean(std::vector<double>* qMean, gsl_rng* r, double C, int timeIndex, bool print);

	double computeLmeanDirMult(std::vector<double>* qMean, double C, int timeIndex, bool print);


    
    //Haplotype reconstruction methods
    struct par {
        int i1;
        int i2;
    };
    void constructFullHaps(std::vector<Sequence> *phs, std::vector<Sequence> *fhs);
    void OverlapStepShared1 (std::vector<std::vector<char> >& haps);
    void ResetHaps (std::vector<int> incl, std::vector< std::vector<char> >& haps);
    void DuplicateStep (std::vector< std::vector<char> >& haps);
    void OverlapStepShared2 (std::vector< std::vector<char> >& haps);
    void OverlapStepNoShare (std::vector< std::vector<char> >& haps);
    void GetStartFinish(std::vector<par>& sf, std::vector< std::vector<char> >& haps);
    void BuildPaths(std::vector<par>& sf, std::vector< std::vector<char> > haps, std::vector< std::vector<char> >& new_haps);
    void AddPath (int prev, int start, std::vector<char> v, std::vector<par>& sf, std::vector< std::vector<char> >& haps, std::vector< std::vector<char> >& new_haps);
    
    
 
    
};


#endif /* analyserPH_hpp */
