//
//  dataPHgen.hpp
//  FluTransmissionProject
//
//

#ifndef dataPHgen_hpp
#define dataPHgen_hpp

//Included dependencies
#include "data.h"
#include "pathPH.h"
#include "sequence.h"
#include "diploidSequence.h"
#include "simParamPH.h"
#include <vector>
#include <iostream>


class DataPH : public Data {

private:
	//Internal variables
	PathPH* path; //Path to data
	std::vector<int> physicalPos; //Only relevant in some cases
	SimParamPH *spPH;

public:
	struct partialHap {
	private:
		Sequence seq;
		std::vector<int> pos;
		std::vector<int> times;
		std::vector<int> obs;
		std::vector<int> contribs;
	
	public:
		void readTraj(std::string &trajString); 
		Sequence getSeq();
		std::vector<int> getPos();
		int getObs(int obsIndex);
		void setSeq(Sequence &s);
		int getNumOfTimePoints();
		void clearContributions();
		void addContrib(int c);
		std::vector<int>* getContribs();
		void print();

	};
   
    
	struct setOfPartialHaps {
	private:
		std::vector<partialHap> phVec;
		std::vector<int> obsTot;
		std::vector<int> pos; //E.g. 120, 337, 455
		std::vector<int> lociCovered; //E.g. if --XXX- then loci covered 2,3,4
        
	public:
		void countObsTot();
		void addPartialHap(partialHap &ph);
		std::vector<int> getPos();
		void setPos(std::vector<int> &p);
	        int getNumOfPartialHaps();
		std::vector<int> getObs(int timeIndex); //Get vector of observations from all PHs in SOPH at time point timeIndex
		void addContrib(int index, int contrib);
	        std::vector<int>* getContribs(int index);
        	void clearContributions();
        	void print();
		std::vector<Sequence> getSequences();
		int getNumOfTimePoints(); //Assumes the times are identical for all PHs
		void setLociCovered(std::vector<int>& lc);
	};
    
private:
	std::vector<setOfPartialHaps> SOPHs;

public:
	DataPH(); //Constructor
	~DataPH(); //Deconstructor
    
	void readData(Path * p);
	void simulateData(SimParam *sp);
	int getNumOfSOPHs();
	int getNumOfTimePoints(); //Assumes the times are identical for all PHs
	setOfPartialHaps* getSOPH(int SOPHindex);

	std::vector<Sequence> getSequences();
	void computeContribs(std::vector<Sequence> &fhs);
    void print();
};

#endif /* dataPH_hpp */
