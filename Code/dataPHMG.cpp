#include "dataPHMG.hpp"
#include <stdio.h>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "misc.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

//Class related methods + constructors
DataPHMG::DataPHMG() { } //Constructor does nothing
DataPHMG::~DataPHMG() {} //Deconstructor

void DataPHMG::readData(Path * p) {
    
    path = * static_cast<PathPHMG *>(p);
    
    //Add data gene by gene
    for(int i=0; i<path.getNumOfPHPaths(); i++) {

	DataPH dph;
        PathPH pph = path.getPHPath(i);
        dph.readData(&pph);

	if(dph.getNumOfSOPHs() > 0) {
		cout << "Data added for gene " << i << "\n";
	}
        

        
	//Check if gene contains any data
	if(dph.getNumOfSOPHs() > 0) {
		genePresent.push_back(true);
	} else {

		cout << "Gene data not present for gene: " << i << "\n";
		genePresent.push_back(false);
	}
        	
	//Add the gene    
	genes.push_back(dph);
	//cout << i << " added: ";
 
    }
    
}

void DataPHMG::simulateData(SimParam *sp) { } //Implement later

int DataPHMG::getNumGenes() {	return (int) genes.size(); }
DataPH* DataPHMG::getGene(int index) { return &(genes[index]); }

bool DataPHMG::getGenePresent(int g) { return genePresent[g]; }
void DataPHMG::setGenePresent(int g, bool b) { genePresent[g] = b; }

