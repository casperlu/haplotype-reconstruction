//Include guard
#ifndef ANAPARAM_H
#define ANAPARAM_H


//Forward declared dependencies

//Included dependencies
#include "sequence.h"
#include <string>

class AnaParam{
protected:
	double c;
	unsigned long int seed;
	std::string pathToFolder;

public:
	//Constructors
	AnaParam();
	~AnaParam();
    
	//Getters and setters
	void setSeed(int s);
	unsigned long int getSeed();
	double getC();
	void setC(double C);
    
	void loadOutputFolder(std::string & fullPathToFolder);
public:
	std::string getOutputFolder();

};
#endif 
