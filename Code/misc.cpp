
//Forward declared dependencies

//Included dependencies
#include "misc.h"
#include <iostream>
#include <gsl/gsl_linalg.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>
#include "fstream"
#include <algorithm>
#include <limits>


using namespace std;

//Defines quantities for numerical integration
const double tol = 1e-06; //Tolerance for numerical integration
const size_t maxEval = 1e07; //MaxEval == 0 signifies unlimited # of evaluations
const unsigned int intOutputDim = 1; //Number of output dimensions must be 1 - it has to be a scaler

//Rescales a vector of doubles to have a sum of 1 and no entry less than 10^-10
void rescale(vector<double> &vec) {
    
	//Get scaling factor
	double sum = 0;
	for (unsigned int i=0; i<vec.size();i++) {
		sum += vec[i];
	}
	if(sum > 0) { //If sum is zero, no need to use scalefactor
		double scaleFactor = 1.0/sum;
    
		//rescale
		for (unsigned int i=0;i<vec.size();i++) {
			vec[i] *= scaleFactor;
		}
	}

	//Check that no values are below 10^-11 as a result of rescaling
	bool changeOccurred = true;
	while(changeOccurred == true) {
	
		changeOccurred = false;

		for (unsigned int i=0; i<vec.size(); i++) {
		
			if(vec[i] < 1e-11) {

				//cout << "Vector < 1e-11: ";
				//printDoubleVector(vec);
				//cout << "\n";

				vec[i] = 1e-10;

				//Define vec2 to be subset of vec that excludes the ith entry
				vector<double> vec2;
				for(unsigned int j=0; j<vec.size(); j++) {
					if(j!=i) {
						vec2.push_back(vec[j]);
					}
				}
				
				if(vec2.size() == 1) { //Can't rescale if of length 1, just keep as is, or bump up to min size
					if(vec2[0] < 1e-11) { vec2[0] = 1e-10; }
				} else {
					rescale(vec2);
				}
			
				//Add new values to vec, but keep ith entry intact	
				for(unsigned int j=0; j<vec2.size(); j++) {
					if(j<i) {
						vec[j]=vec2[j];
					} else {
						vec[j+1]=vec2[j];
					}
				}

				rescale(vec);
				changeOccurred = true;
				break; //Leave for loop and start again
			}
		}
	}	
}
//Method for printing vectors of doubles
void printDoubleVector(vector<double> &vec) {
	cout.precision(25);
    if(vec.size() > 0) {
        cout << "Vector = { ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            cout << vec[i] << " , ";
        }
        cout << vec[vec.size()-1] << " }\n";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
	cout.precision(6); //Change back
}

string printDoubleVectorToString(vector<double> &vec) {
    
    string output;
    if(vec.size() > 0) {
        output += "{ ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            output += to_string(vec[i]);
		output += " , ";
        }
        output += to_string( vec[vec.size()-1]);
	output += " }";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
    return output;
}


//Method for printing vectors of ints
void printIntVector(vector<int> &vec) {
    if(vec.size() > 0) {
        cout << "Vector = { ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            cout << vec[i] << " , ";
        }
        cout << vec[vec.size()-1] << " }\n";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
}


int sumOfVector(vector<int> intVec) {
    
    int tot = 0;
    for(unsigned int i=0; i<intVec.size(); i++) {
        tot+= intVec[i];
    }
    return tot;
}

double sumOfVector(vector<double> doubleVec) {
    
    double tot = 0;
    for(unsigned int i=0; i<doubleVec.size(); i++) {
        tot+= doubleVec[i];
    }
    return tot;
}

vector<double> subtractVectorsDouble(vector<double> &a, vector<double> &b) {
    
    vector<double> result;
    //Assume equal length
    for(unsigned int i=0; i<a.size(); i++) {
        result.push_back(a[i]-b[i]);
    }
    return result;
}

double logMultivariateNormalPDF(vector<double> &x, vector<double> &mu, gsl_matrix *sigma) {
    
    //Set up permutation and matrix copy
    int dim= (int) sigma->size1;
    vector<double> xMinusMu = subtractVectorsDouble(x,mu);
    gsl_permutation *p = gsl_permutation_calloc(dim);
    int signum;
    gsl_matrix * tmp_ptr = gsl_matrix_calloc(dim,dim);
    gsl_matrix_memcpy(tmp_ptr,sigma); //Copy elements of sigma into tmp_ptr
    
    //Get LU decomposition
    gsl_linalg_LU_decomp(tmp_ptr,p,&signum);
    
    //Get determinant
    double det = gsl_linalg_LU_det(tmp_ptr, signum);
//	cout <<  "Determinant: " << det << "\n";
//	if(det==0) { det=1e-10; }
    
    //Get inverse
    gsl_matrix *sigmaInv = gsl_matrix_alloc(dim,dim);
    gsl_set_error_handler_off();
    int status = gsl_linalg_LU_invert(tmp_ptr,p,sigmaInv);
    if (status) {
        
	//Clean up, then return negative infinity
	gsl_matrix_free(sigmaInv);
	gsl_matrix_free(tmp_ptr);
	gsl_permutation_free(p);
    

        cout << "Matrix not positive definite. Returning probability of neg infinity.\n";
//	cout << "Determinant should be 0 in this case. Determinant was: " << det << "\n";
        return  -numeric_limits<double>::max();
    }
    
    
    //double logPrefactor= -0.5*log(pow(2*M_PI,dim)*det);
    double logPrefactor= -0.5*dim*log(2*M_PI) -0.5*log(det);
    
    //Convert xMinusMu to a gsl_vector
    gsl_vector *xMinusMuGSL = gsl_vector_alloc(dim);
    for(unsigned int i=0;i<xMinusMu.size();i++) {
        gsl_vector_set(xMinusMuGSL,i,xMinusMu[i]);
    }
    
    //Perform matrix*vector multiplication
    gsl_vector *sigmaInvXminusMu = gsl_vector_alloc(dim);
    gsl_blas_dgemv(CblasNoTrans,1.0,sigmaInv,xMinusMuGSL,0,sigmaInvXminusMu);
    
    //Perform vector*vector multiplication
    double dotProd;
    gsl_blas_ddot(xMinusMuGSL,sigmaInvXminusMu,&dotProd);
    
	//Clean up
	gsl_vector_free(xMinusMuGSL);
	gsl_vector_free(sigmaInvXminusMu);
	gsl_matrix_free(sigmaInv);
	gsl_matrix_free(tmp_ptr);
	gsl_permutation_free(p);
    
	return logPrefactor -0.5*dotProd;
}

double logMultivariateNormalPDFPrint(vector<double> &x, vector<double> &mu, gsl_matrix *sigma) {
	
	cout << "Input mu:\n";
	printDoubleVector(mu);
	cout << "Input sigma:\n";
	printMatrixMathematica(sigma);    

    //Set up permutation and matrix copy
    int dim= (int) sigma->size1;
    vector<double> xMinusMu = subtractVectorsDouble(x,mu);
	cout << "xMinusMu: "; printDoubleVector(xMinusMu);

    gsl_permutation *p = gsl_permutation_calloc(dim);
    int signum;
    gsl_matrix * tmp_ptr = gsl_matrix_calloc(dim,dim);
    gsl_matrix_memcpy(tmp_ptr,sigma); //Copy elements of sigma into tmp_ptr
    
    //Get LU decomposition
    gsl_linalg_LU_decomp(tmp_ptr,p,&signum);
    
    //Get determinant
    double det = gsl_linalg_LU_det(tmp_ptr, signum);
	cout << "Determinant: " << det << "\n";
    
    //Get inverse
    gsl_matrix *sigmaInv = gsl_matrix_alloc(dim,dim);
    gsl_set_error_handler_off();
    int status = gsl_linalg_LU_invert(tmp_ptr,p,sigmaInv);
    if (status) {
        
	//Clean up, then return negative infinity
	gsl_matrix_free(sigmaInv);
	gsl_matrix_free(tmp_ptr);
	gsl_permutation_free(p);
    

        cout << "Matrix not positive definite. Returning probability of neg infinity.\n";
        return  -numeric_limits<double>::max();
    }
    
	cout << "Sigma inverse: "; printMatrix(sigmaInv);
    
    //double logPrefactor= -log(sqrt(pow(2*M_PI,dim)*det));
    double logPrefactor= -0.5*dim*log(2*M_PI) -0.5*log(det);
	cout << "logPrefactor: " << logPrefactor << "\n";
    
    //Convert xMinusMu to a gsl_vector
    gsl_vector *xMinusMuGSL = gsl_vector_alloc(dim);
    for(unsigned int i=0;i<xMinusMu.size();i++) {
        gsl_vector_set(xMinusMuGSL,i,xMinusMu[i]);
    }
    
    //Perform matrix*vector multiplication
    gsl_vector *sigmaInvXminusMu = gsl_vector_alloc(dim);
    gsl_blas_dgemv(CblasNoTrans,1.0,sigmaInv,xMinusMuGSL,0,sigmaInvXminusMu);
    
    //Perform vector*vector multiplication
    double dotProd;
    gsl_blas_ddot(xMinusMuGSL,sigmaInvXminusMu,&dotProd);
	cout << "dotProduct: " << dotProd << "\n";    

    //Clean up
    gsl_vector_free(xMinusMuGSL);
    gsl_vector_free(sigmaInvXminusMu);
    gsl_matrix_free(sigmaInv);
    gsl_matrix_free(tmp_ptr);
    gsl_permutation_free(p);
    
    return logPrefactor -0.5*dotProd;
}

//Dirichlet multinomial function taking vectors of observations and inferred frequencies as inputs
double logDirMultProb(vector<double> &alpha, vector<int> &x, int& n) {
    

	double sumAlpha = 0;
	double sum = 0;
	for(unsigned int i=0; i<alpha.size(); i++) {
		sumAlpha += alpha[i];
		sum += gsl_sf_lngamma(x[i]+alpha[i]) - gsl_sf_lngamma(x[i]+1) - gsl_sf_lngamma(alpha[i]);
	}
	double logL = gsl_sf_lngamma(n+1) + gsl_sf_lngamma(sumAlpha) - gsl_sf_lngamma(n+sumAlpha) + sum;

	return(logL);
}

double logBetaBinProb(double alpha, double beta, int x, int n) {


	return(gsl_sf_lngamma(n+1)+gsl_sf_lngamma(x+alpha)+gsl_sf_lngamma(n-x+beta)+gsl_sf_lngamma(alpha+beta)-gsl_sf_lngamma(x+1)-gsl_sf_lngamma(n-x+1)-gsl_sf_lngamma(n+alpha+beta)-gsl_sf_lngamma(alpha)-gsl_sf_lngamma(beta));
}


//Integration parameters
struct parameters {

	double mu;
	double var;
	double stDev;
	vector<double> muVec;
	gsl_matrix* Sigma;

};

void printMatrix(gsl_matrix *A) {
    
    int dim1= (int) A->size1;
    int dim2= (int) A->size2;
    
    for (int i = 0; i < dim1; i++)  /* OUT OF RANGE ERROR */
        for (int j = 0; j < dim2; j++)
            printf ("m(%d,%d) = %g\n", i, j, gsl_matrix_get (A, i, j));
    
    
}

//Outputs matrix to screen in mathematica format A = {{A11,A12,A13},{A21,A22,A23},{A31,A32,A33}}
void printMatrixMathematica(gsl_matrix *A) {
    
    int dim1= (int) A->size1;
    int dim2= (int) A->size2;
    

	cout << "{";
	for (int i = 0; i < dim1; i++) {
		cout << "{";
		for (int j = 0; j < dim2; j++) {
			cout.precision(25);
			cout << gsl_matrix_get(A,i,j);
			cout.precision(6); //Back to default
			if(j<dim2-1) {
				cout << ",";
			}
  		}
		cout << "}";
		if(i<dim1-1) {
			cout << ",";
		}
	}
	cout << "}\n"; 
}


bool identicalSeqs(Sequence & a, Sequence & b) {
    
    int lengthA = a.getLength();
    if(lengthA != b.getLength()) {
        cout << "ERROR in identicalSeqs: Sequences a and b are of different lengths!!\n";
        cout << "a = ";
        a.print();
        cout << "b = ";
        b.print();
        cout << "Returning false.\n";
        return false;
    } else {
        
        for(int i=0;i<lengthA;i++) {
            if(a.getBase(i) != b.getBase(i)) {
                return false;
            }
        }
        return true;
    }
    
}

bool identicalIntVectors(std::vector<int> & a, std::vector<int> & b) {

	int lengthA = (int) a.size();
	
	if(lengthA != (int) b.size()) {
		cout << "ERROR in identicalIntVectors: vectors a and b are of different lengths!!\n";
		cout << "a = ";
		printIntVector(a);
		cout << "b = ";
		printIntVector(b);
        cout << "Returning false.\n";
        return false;
    } else {
        
		for(int i=0;i<lengthA;i++) {
            if(a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
   
}

//Method for adding a random small delta to a random frequency index
void addRandom(vector<double> &oldVec, vector<double> &newVec, double delta, gsl_rng *r) {
    
    //Set newFreqs to oldFreqs values
    newVec = oldVec;

	//Update index of newFreq
	int index = gsl_rng_uniform_int(r,oldVec.size());
	double newValue = newVec[index] + delta*(gsl_rng_uniform_pos(r) - 0.5);
	if(newValue < 1e-11) {
		newValue = 1e-10;
	}
	newVec[index] = newValue;

	rescale(newVec);
}

//Method for adding a random small delta to a random frequency index
void addRandom(vector<double> & vec, double delta, gsl_rng *r) {
    
	//Update index of newFreq
	int index = gsl_rng_uniform_int(r,vec.size());
	double newValue = vec[index] + delta*(gsl_rng_uniform_pos(r) - 0.5);
	if(newValue < 1e-11) {
		newValue = 1e-10;
	}
	vec[index] = newValue;

	rescale(vec);
}



//Check if a file exists. Give full path to file
bool fileExists (string& fileName) {
    ifstream f(fileName.c_str());
    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    }
}

//Mean and var are reduced by 1 dimension (namely the last (kth) dimension)
vector<double> computeAlpha(vector<double>& mean, gsl_matrix* var, int& n) {


	//Simple alpha_0 created from the 0,0 component only
	//q_i,j = var_i,j/(mean_i * (delta_i,j - mean_j/n))
//	double q_00 = gsl_matrix_get(var,0,0)/(mean[0]*(1-mean[0]/n));
//	cout << "q_00: " << q_00 << "\n";
//	double alpha_0 = (n-q_00)/(q_00-1);
//	cout << "alpha_0: " << alpha_0 << "\n";

	//Alpha_0 created from the average of all alpha_0_i,j
	double alpha_0_sum = 0;
	int numIncluded = 0;
	for(unsigned int i=0; i<mean.size(); i++) {
		for(unsigned int j=0; j<mean.size(); j++) {

			int KroneckerDelta = 0;
			if(i==j) { KroneckerDelta= 1; }
			double q_ij = gsl_matrix_get(var,i,j)/(mean[i]*(KroneckerDelta-mean[j]/n));
			double alpha_0_ij = (n-q_ij)/(q_ij-1);
//			cout << "alpha_o_" << i << "," << j <<": " << alpha_0_ij << "\n";
			if(alpha_0_ij > 0) {
				alpha_0_sum += alpha_0_ij;
				numIncluded++;
			}
		}
	}
	cout << "alpha_0_sum: " << alpha_0_sum << "\n";
	//double alpha_0 = alpha_0_sum/(mean.size()*mean.size());
	double alpha_0 = alpha_0_sum/numIncluded;
	cout << "alpha_0: " << alpha_0 << "\n";
//	if(alpha_0<=0) { alpha_0 = 0.01; }
	if(alpha_0_sum == 0) {
	
		for(unsigned int i=0; i<mean.size(); i++) {
			for(unsigned int j=0; j<mean.size(); j++) {

				int KroneckerDelta = 0;
				if(i==j) { KroneckerDelta= 1; }
				cout << "var_" << i << "_" << j << ": " << gsl_matrix_get(var,i,j) << "\n";
				cout << "mean_" << i << ": " << mean[i] << "\n";
				cout << "mean_" << j << ": " << mean[j] << "\n";
				double q_ij = gsl_matrix_get(var,i,j)/(mean[i]*(KroneckerDelta-mean[j]/n));
				double alpha_0_ij = (n-q_ij)/(q_ij-1);
				cout << "alpha_o_" << i << "," << j <<": " << alpha_0_ij << "\n";
				if(alpha_0_ij > 0) {
					alpha_0_sum += alpha_0_ij;
					numIncluded++;
				}
			}
		}

	}

	
	//Create full dimensional alpha
	vector<double> alpha;
	double sumMean = 0;
	for(unsigned int i=0; i<mean.size(); i++) {

		alpha.push_back(alpha_0*mean[i]/n);
		sumMean += mean[i];
	}
	//Do kth dimension
	alpha.push_back(alpha_0*(n-sumMean)/n);
	cout << "alpha: "; printDoubleVector(alpha);
	cout << "mean: "; printDoubleVector(mean);
	cout << "sumMean: " << sumMean << " n: " << n << "\n";
	
	return(alpha);

}

//Takes an already subsetted vector x !
void constructMatrixM(vector<double> x, gsl_matrix *M) {

    //Create subset of vec
    int dim = (int) x.size();

	//Hardcode the very simple cases as it gives a considerable speed-up. See mMatrixChecker.cpp.
	if(dim==1) { //Very simple case, if x={x1} then M = {{ x1-x1^2 }}, hardcode for efficiency. About 1.5 times as fast

		double value = x[0] - x[0]*x[0];
		gsl_matrix *mat = gsl_matrix_alloc(dim,dim);
		gsl_matrix_set(mat,0,0,value);
		gsl_matrix_memcpy(M,mat); //Copy mat into M, entry by entry
		gsl_matrix_free(mat); //Clean up
		return;

	} else if(dim==2) { //Also simple case. Here M[{x1,x2}] = {{x1 - x1^2, -x1 x2}, {-x1 x2, x2 - x2^2}}. About 1.75 times as fast.

		gsl_matrix *mat = gsl_matrix_alloc(dim,dim);
		gsl_matrix_set(mat,0,0,x[0]-x[0]*x[0]);
		gsl_matrix_set(mat,1,0,-x[0]*x[1]);
		gsl_matrix_set(mat,0,1,-x[0]*x[1]);
		gsl_matrix_set(mat,1,1,x[1]-x[1]*x[1]);
		gsl_matrix_memcpy(M,mat); //Copy mat into M, entry by entry
		gsl_matrix_free(mat); //Clean up
		return;

	}
    
    //Create diagonal matrix with elements of x
    gsl_matrix *diagX = gsl_matrix_alloc(dim,dim);
    for(int j=0; j<dim; j++) {
        for(int k=0; k<dim; k++) {
            
            if(j==k) {
                gsl_matrix_set(diagX,j,k,x[j]);
            } else {
                gsl_matrix_set(diagX,j,k,0);
            }
        }
    }
    
    /*
     / Create outer product of x*x^transpose.
     / This method does not exist in CBLAS library, but can be obtained otherwise.
     / In particular, Outer(a,b) = Matrix(columns=a) * DiagonalMatrix(b).
     */
    //Create matrices
    gsl_matrix *A = gsl_matrix_alloc(dim,dim);
    for(int j=0; j<dim; j++) {
        for(int k=0; k<dim; k++) {
            gsl_matrix_set(A,j,k,x[j]);
        }
    }
    
    //Multiply them and store in XX
    gsl_matrix *XX=gsl_matrix_alloc(dim,dim);
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                    1.0, A, diagX,
                    0.0, XX);
    
    //Create M=diag(x) -xxp^t
    gsl_matrix_sub(diagX,XX); //diagX=diagX-XX, i.e. store result in diagPb
    
    //Clean up
    gsl_matrix_memcpy(M,diagX); //Copy to M, entry by entry
    gsl_matrix_free(diagX);
    gsl_matrix_free(A);
    gsl_matrix_free(XX);   
}




