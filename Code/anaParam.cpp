
//Forward declared dependencies

//Included dependencies
#include "anaParam.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include "misc.h"
#include <limits>


using namespace std;

//Constructors
AnaParam::AnaParam() : c(200), seed(1) {

	//Do nothing
}
AnaParam::~AnaParam() { } //Deconstructor


//Getters
unsigned long int AnaParam::getSeed() { return seed; }
void AnaParam::setSeed(int s) { seed = s; }
double AnaParam::getC() { return c; }
void AnaParam::setC(double C) { c = C; }

void AnaParam::loadOutputFolder(string & fullPathToFolder) { pathToFolder=fullPathToFolder; }

string AnaParam::getOutputFolder() {
    if(pathToFolder.length() > 0) { return pathToFolder; }
    else {
        cout << "ERROR: Path to output folder not loaded. Expect segmentation fault";
        return pathToFolder;
    }
}


